<?php 
    require('vendor/autoload.php');
    use Mollie\Api\MollieApiClient;
    $key = "test_FzUWJeBK7dk7h7EDQjqDu6sjN7E4eG";
    $mollie = new  MollieApiClient();
    $mollie->setApiKey($key);
    
    $payment = $mollie->payments->create([
        "amount" => [
            "currency" => "EUR",
            "value" => "13.37"
        ],
        "description" => "My first payment",
        "redirectUrl" => "https://webhook.site/f7d2eea9-4e93-46fd-a348-9e99fef371d3",
        "webhookUrl" => "https://webhook.site/f7d2eea9-4e93-46fd-a348-9e99fef371d3"
    ]);
    $id = $payment->id;
    header("Location: " . $payment->getCheckoutUrl(), true, 303);
    
?>