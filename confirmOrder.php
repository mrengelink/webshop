<?php 
    include('dbHandler.php');
    require('vendor/autoload.php');
    use Mollie\Api\MollieApiClient;
    session_start();
    if(!isset($_SESSION['ID'])){
        header('location: login.php');
    }

    if(!isset($_SESSION['products'])){
        header('location: game_shop.php');
    }
    
    $products = $_SESSION['products'];
    $userID = $_SESSION['ID'];
    $title = "Confirm Order";

    $transaction = [
        [
            'sql'=> "INSERT INTO orders(USER_ID,STATUS)VALUES(:userID,:status)",
            'var' => [
                'userID' => $userID,
                'status' => "placed"
            ]
        ]
    ];

    foreach($products as $product){
        $sql = "INSERT INTO order_rows(ORDER_ID,PRODUCT_ID,NUM_PRODUCTS,PRICE)VALUES(:LAST_ID,:productID,1,:price)";
        $vars = [
            'productID' => $product['product_id'],
            'price'=> $product['price'],
            'LAST_ID' => ''
        ];
        $object = [
            'sql'=> $sql,
            'var'=> $vars
        ];
        array_push($transaction,$object);  
    }
    DbHandler::MultiQuery($transaction);
    

    $sql = "SELECT SUM(order_rows.PRICE) as total FROM order_rows WHERE ORDER_ID=(SELECT MAX(ID) FROM orders WHERE USER_ID=?)";
    $results = DbHandler::Query($sql,[$userID]);
    foreach($results as $result){

        $price = $result['total'];
    }

    $_SESSION['products'] = null;
    unset($_SESSION['products']);

    $date = date("Y-m-d");
    $desc = "Order: $date";
    echo $price . " " . $desc;
    $key = "test_FzUWJeBK7dk7h7EDQjqDu6sjN7E4eG";
    $mollie = new  MollieApiClient();
    $mollie->setApiKey($key);
    $payment = $mollie->payments->create([
        "amount" => [
            "currency" => "EUR",
            "value" => $price
        ],
        "description" => "$desc",
        "redirectUrl" => "https://a86ce82d.ngrok.io/index.php",
        "webhookUrl" => "https://a86ce82d.ngrok.io"
    ]);
    $id = $payment->id;
    $status = $payment->status;
    $sql = "UPDATE orders SET MOLLIE_ID=:id, STATUS=:status WHERE ID=(SELECT MAX(t.ID) FROM (SELECT ID, USER_ID FROM orders) t WHERE t.USER_ID=:userID)";
    $result = DbHandler::Query($sql,[
        'id' =>$id,
        'status' => $status,
        'userID'=>$userID]);
    header("Location: " . $payment->getCheckoutUrl(), true, 303);
?>
