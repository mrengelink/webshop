<?php
    session_start();
    include('dbHandler.php');

    $title = "Logs";

    $id = isset($_SESSION['ID']) ? $_SESSION['ID'] : "not logged in";
    $sql = "SELECT * FROM logs INNER JOIN users ON logs.EMAIL=users.EMAIL WHERE users.ID=?";
    $result = DbHandler::Query($sql,[$id]);
    
    foreach($result as $row){
        $msg = $row["MSG"];
        $time = $row["TIME"];
        $ip = $row['IP'];
        $element = "<div class='logContainer'><div class='logValue'>$time</div><div class='logValue'>$ip</div><div class='logValue'>$msg</div></div>";
        $logs .= "\n" . $element;
    }

    include('html/head.html');
    include('header.php');
    include('html/logs.html');
    include('html/footer.html');
?>