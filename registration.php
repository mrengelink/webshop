<?php
    include_once("util.php");
    include_once("dbHandler.php");

    $title = "Registration"; 
    $email = $password = $password2 = $firstname = $lastname = '';
    if($_SERVER['REQUEST_METHOD'] == "POST") {
        if(checkForm()){
            $token = uniqid();
            createUser($email,$password, $firstname, $lastname, $token);
            mail($email,"verification Account Test.com","To complete your registration please click the link below </br> <a href='127.0.0.1:9999/verification.php?token=" . $token . ">",'From: mrengelink@gmail.com');
        }else {
            echo "fields error";
        };
    }

    function checkForm() {
        global $email, $password,$password2, $firstname, $lastname;

        $fields = array("email","password","password2","firstname","lastname");

        $emptyFields = Util::getEmptyFields( $fields );
        if(count($emptyFields) > 0){
            echo "empty fields";
            return false;
        }

        for($i=0; $i < count($fields); $i++) {
            ${$fields[$i]} = Util::validateInput( $_POST[ $fields[$i] ] );
        }

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            echo "email error";
            return false;
        }

        if($password != $password2){
            echo "password error";
            return false;
        }  
    
        return true;
    }

    function createUser($email,$password,$firstName,$lastName,$token){
        $password = password_hash($password,PASSWORD_DEFAULT);
        $sql = "INSERT INTO users (EMAIL,PASS,FIRSTNAME,LASTNAME,TOKEN) VALUES (?,?,?,?,?);";
        $result = DbHandler::Query($sql,[$email,$password,$firstName,$lastName,$token]);
    }

    include('html/head.html');
    include('header.php');
    include('html/registration.html');
    include('html/footer.html');
?>
