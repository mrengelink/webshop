<?php
    session_start();
    include_once('util.php');
    include_once("dbHandler.php");
    $title = "Login";
    $email = $password = "";
    $emailError = $passwordError = "";

    if($_SERVER['REQUEST_METHOD'] == "POST") {
        if(checkForm()){
            $sql= "SELECT ID,PASS,EMAIL,LOGIN_ATTEMPS,ACTIVE FROM users WHERE EMAIL=?";
            $result = DbHandler::Query($sql,[$email]);
            if(count($result) === 1){
                $r = $result[0];
                if($r["ACTIVE"]){
                    if(password_verify($password,$r["PASS"])) {
                        onCorrectInfo($r);
                    }else {
                        onIncorrectInfo($email,$r["LOGIN_ATTEMPS"]);
                    }
                }else {
                    //TODO: change this;
                    echo "account is locked. Please contact admin.";
                }
            }else {
                onIncorrectInfo($email);
            }
        }
    }
    
    function onCorrectInfo ($r) {
        $_SESSION['ID'] = $r['ID'];
        $_SESSION['EMAIL'] = $r['EMAIL'];
        $id = $_SESSION['ID'];
        $sql= "UPDATE users SET LOGIN_ATTEMPS = 0, LAST_LOGIN = CURRENT_TIMESTAMP WHERE ID=?";
        DbHandler::Query($sql,[$id]);
        createLog("Login Success"); 
        header('Location: game_shop.php');
    }

    function onIncorrectInfo ($email,$attemps = null) {
        $sql="UPDATE users SET LOGIN_ATTEMPS = LOGIN_ATTEMPS+1 WHERE EMAIL =?";
        $r = DbHandler::Query($sql,[$email]);
        $msg = "Login Failed: ";
        echo "Email or password incorrect";
        
        if($attemps < 5) {
            echo " you have " . (5 - $attemps ) . ' left before account is locked.';
            $msg = $msg . (5 - $attemps) . " attemps left"; 
        }else{
            //lock account
            echo " account is locked";
            $msg = $msg . " account is locked";
            $sql = "UPDATE users SET ACTIVE = 0 WHERE EMAIL=?"; 
            DbHandler::Query($sql,[$email]);
            if(count($r) > 0){
                mail("$email","Account is locked","Due to multiple false login attemps you're account has been locked for security reasons.");
            }
        }
        createLog($msg);
    }

    function checkForm() {
        global $email, $password , $emailError , $passwordError; 
        if(empty($_POST["email"])){
            if(empty($_POST["email"])) {
                $emailError = "Email is required.";
            }
            if(empty($_POST["password"])){
                $passwordError = "Password is required.";
            }
            return false;
        }else{
            $email = Util::validateInput($_POST["email"]);
            if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                $emailError = "Invalid email format";
                return false;
            }
            $password = Util::validateInput($_POST["password"]);
        }
        return true;
    }

    function createLog($msg) {
        global $email;
        $ip = $_SERVER["REMOTE_ADDR"];
        $sql = "INSERT INTO logs (EMAIL,IP,MSG) VALUES (?,?,?);";
        DbHandler::Query($sql,[$email,$ip,$msg]);
    }

    include('html/head.html');
    include('header.php');
    include('html/login.html');
    include('html/footer.html');
?>