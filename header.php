<?php 
    include_once('dbHandler.php');
    session_start();
    $num = 0;
    if(isset($_SESSION['products'])){
        $num = count($_SESSION['products']);
    }


    if(!isset($_SESSION['ID'])){
        $nav = <<<EOT
        <div >
            <a class="navButton" href='registration.php'>Sign Up</a>
        </div>
        <div>
            <a class="navButton" href='login.php'>Login</a>
        </div>
        
EOT;
    }else{
        $email = $_SESSION['EMAIL'];
        $id = $_SESSION['ID'];
        $sql = "SELECT ID FROM profiles WHERE USER_ID=?";
        $result = DbHandler::Query($sql,[$id]);
        $row = $result[0];
        $profile_id= $row['ID'];
        $nav = <<<EOT
        
        <div style="position:relative">
        <div id="userButton" class="buttonCursor navButton" >
            $email
        </div>
        <div id="userWrapper">
            <div id="userContainer" class="slideUp">
                <div id="profileButton" class="buttonCursor userButton" data-code='$profile_id'>
                    Profile
                </div>
                <div id="shopEditButton" class="buttonCursor userButton">
                    Manage Shop
                </div>
            </div> 
        </div>
    </div>
        <div id='logoutButton' class="buttonCursor navButton">Logout</div>

EOT;
    }
    include('html/header.html');
?>