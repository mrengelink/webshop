<?php
    include_once('dbHandler.php');

    if($_SERVER['REQUEST_METHOD'] == "GET"){
        $gameID= $_GET['id'];
        $sql = "SELECT * FROM games WHERE id=?";
        $result = DbHandler::Query($sql,[$gameID]);
        $r = $result[0];
        $gameTitle = $r['TITLE'];
        $desc = $r['DESCRIPTION'];
        $title = $gameTitle;
        $price = $r['PRICE'];

        $about = $r['ABOUT'];

        $sql = "SELECT MEDIA_TYPE,MEDIA_URL FROM store_media WHERE GAMEID=?";
        $result = DbHandler::Query($sql,[$gameID]);

        $media = "";
    
        foreach($result as $row){
            $url = $row['MEDIA_URL'];
            if($row['MEDIA_TYPE'] == "VIDEO"){
                $media .= "<video class='storeMedia' controls src='$url' > </video>";
            }else {
                $media .= "<img class='storeMedia' src='$url'>";
            }
        }
    }

    include('html/head.html');
    include('header.php');
    include('html/game_info.html');
    include('html/footer.html');
?>