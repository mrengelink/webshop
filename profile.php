<?php 
    session_start();
    include_once('dbHandler.php');
    if(isset($_GET['id'])) {
        $profileID = $_GET['id'];
        $sql = "SELECT USER_ID,NAME, AVATAR , ABOUT FROM profiles WHERE ID=?";
        $result = DbHandler::Query($sql,[$profileID]);
        $id= "";
        $user = "Unknown Profile";
        $title = $user;
        $image = "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/fe/fef49e7fa7e1997310d705b2a6158ff8dc1cdfeb_full.jpg";
        $about = "No information given.";
        foreach($result as $row){
            $user = $row['NAME'];
            $title = $user . " | Profile";
            $image = $row['AVATAR'];
            $id = $row['USER_ID'];
            if($row['ABOUT'] != ''){
                $about = $row['ABOUT'];
            }
        }
        $button = "";
        if(isset($_SESSION['ID'])) {
            if($id == $_SESSION['ID']) {
                $button = "<div id='editProfile'>Edit Profile</div>";
            }
            
        }
        
    }

    include('html/head.html');
    include('header.php');
    include('html/profile.html');
    include('html/footer.html');
?>