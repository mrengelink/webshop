<?php 
    include('dbHandler.php');


    $userID = $_SESSION['ID'];
    $sql= "SELECT DATE, SUM(PRICE), STATUS FROM ";
    $results = DbHandler::Query($sql,[]);

    $transactions = "<li>No transactions</li>";
    if(count($results) > 0) {
        $transactions = "";
        foreach($results as $result) {
            $date = $result['DATE'];
            $price = $result['PRICE'];
            $status = $result['STATUS'];
            $transactions .= "<li><div><span>$date</span><span>$price</span><span>$status</span></div></li>";
        }
    }
    

    include('html/head.html');
    include('header.php');
    include('html/transactionHistory.html');
    include('html/footer.html');
?>