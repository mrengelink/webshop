<?php 
    session_start();
    include_once('util.php');
    include_once('dbHandler.php');
    $title="Edit Products";
    /*****************************
     *  Search
     ****************************/
    if(isset($_GET['q'])){
        $q = Util::validateInput($_GET['q']);
        $sql = "SELECT * FROM games WHERE title LIKE ? LIMIT 5";
        $q= "%$q%";
        $result = DbHandler::Query($sql,[$q]);
        $response = "";

        foreach($result as $r) {
            $id= $r['ID'];
            $title = $r['TITLE'];
            $desc = $r['DESCRIPTION'];
            $response .= "<li><div>$id</div><div>$title</div></li>";
        }
        die($response);
    }

    /*****************************
     * Edit product
     *****************************/
    if(isset($_POST['method']) && $_POST['method'] == 'edit') {
        foreach($_POST as $k => $v) {
            if($k =='method'){
                break;
            }
            $variables[$k] = $v;
        }
        $variables['id'] = $_SESSION['selectedProductID'];
        $sql= "UPDATE games SET  title=:title , description=:description, about=:about, price=:price WHERE id=:id";
        $result = DbHandler::Query($sql,$variables);
        // if(count($result) > 0) {
        //     echo "Edit Succes";//die("Edit Failed");
        // }
        //die("Edit succes");
    }

    /*****************************
     *  Delete product
     ****************************/
    if(isset($_POST['delete'])&& $_POST['delete'] == 1) {
        $variables['id'] = $_SESSION['selectedProductID'];
        $sql="DELETE FROM games WHERE ID=:id";
        $result = DbHandler::Query($sql,$variables);
        $_SESSION['selectedProductID'] = null;
        unset($_SESSION['selectedProductID']);
    }

    /**************************
     * Add product
     ****************************/
    if(isset($_POST['title']) && isset($_POST['method']) && $_POST['method'] == 'add'){
        foreach($_POST as $k => $v){
            if($k == "method"){
                break;
            }
            $variables[$k] = Util::validateInput($v);
        }
        $variables['owner_id'] = $_SESSION['ID'];
        $sql = "INSERT INTO games (TITLE,PRICE,THUMB,ABOUT,DESCRIPTION,OWNER_ID) VALUES (:title,:price,:thumb,:about,:desc,:owner_id)";
        $result = DbHandler::Query($sql,$variables);
        if(count($result) > 0){
            echo 'succes';
        }else{
            echo 'failed';
        }
    }
    /******************************
    *   Select Product 
    ******************************/ 
    if(isset($_GET['product_id'])) {
        $productID  = Util::validateInput($_GET['product_id']);
        $sql = "SELECT * FROM games WHERE ID=?";
        $results = DbHandler::Query($sql,[$productID]);
        $response = "";
        $_SESSION['selectedProductID'] = $_GET['product_id'];
        foreach($results as $result) {
            $ptitle = $result['TITLE'];
            $about = $result['ABOUT'];
            $desc = $result['DESCRIPTION'];
            $price = $result['PRICE']; 

            $response .= <<<EOT
            <div class="selectProduct">
                <form id="editform" class="editProduct" method="post" action="shop_edit.php">
                    <div >
                        <label> Title </label>
                        <input name="title" type="text" value="$ptitle">
                    </div>
                    <div>
                        <label> Price </label>
                        <input name="price" type="number" value="$price">
                    </div>
                    <div>
                        <label> Description </label>
                        <textarea name="description" type="text">$desc</textarea>
                    </div>
                    <div>
                        <label> About </label>
                        <textarea class="about" name="about" type="text">$about</textarea>
                    </div>
                    
                    
                    <input name="method" type="hidden" value="edit">
                </form>
                <div class="btnContainer">
                    <div class="AccountTitle"></div>
                    <div id="editButton" class="buttonCursor">Edit</div>
                </div>
                <div class="btnContainer">
                    <div class="AccountTitle"></div>
                    <div id="cancelButton" class="buttonCursor">Cancel</div>
                </div>
                <div class="AccountTitle"></div>
                <div id="deleteButton" class="buttonCursor">Delete product</div>
            </div>
EOT;
        }
        die($response);
    }


    /******************************
    * Get All products from Owner
    ******************************/
    $id = $_SESSION['ID'];
    $sql = "SELECT * FROM games WHERE OWNER_ID=?";
    $results = DbHandler::Query($sql,[$id]);
    $products = "<div>No products</div>";
    if(count($results) > 0){
        $products = <<<EOT
        <div>
            Select Product
            <input id="searchField">
        </div>
        <ul id="productList">
EOT;
    
        foreach($results as $result) {
            $ptitle = $result['TITLE'];
            $price = $result['PRICE'];
            $productID = $result['ID'];
            $products .= "<li class='product buttonCursor' data-code='$productID'><span>$ptitle</span><span>&euro;$price</span></li>";
        }
        $products .= "</ul>";
    }
    
    include("html/head.html");
    include("header.php");
    include('html/addProduct.html');
    include('html/footer.html')

?>