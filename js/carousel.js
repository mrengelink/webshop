let isSliding = false;
let elements = document.getElementsByClassName("storeMedia");
let nextButton = document.getElementById("nextButton");
let previousButton = document.getElementById("previousButton");
if (elements.length < 1) {
    nextButton.style.display = 'none';
    previousButton.style.display = 'none';
}

var index = elements.length - 1;
nextButton.addEventListener('click', Next);
previousButton.addEventListener('click', Previous);

elements[index].style.animation = "slideRightToCenter 0.0s forwards linear";
//TODO:move to other file.
let addToCartElement = document.getElementById("addToCart");
if (addToCartElement != null) {
    addToCartElement.addEventListener('click', addToCart);
}

function stopVideo() {
    let elements = document.getElementsByTagName("video");
    for (let e of elements) {
        e.pause();
    }
}

function handler(e) {
    e.target.removeEventListener(e.type, arguments.callee);
    isSliding = false;
}

function Previous() {
    if (!isSliding) {
        isSliding = true;
        stopVideo();
        elements[index].addEventListener('animationend', handler);
        elements[index].style.animation = "slideCenterToRight 0.5s forwards linear";
        index++;
        if (index == elements.length) {
            index = 0;
        }
        elements[index].style.animation = "slideRightToCenter 0.5s forwards linear";
    }
}

function Next() {
    if (!isSliding) {
        isSliding = true;
        stopVideo();
        elements[index].addEventListener('animationend', handler);
        elements[index].style.animation = " slideCenterToLeft 0.5s forwards linear";
        index--;
        if (index < 0) {
            index = elements.length - 1;
        }
        elements[index].style.animation = "slideLeftToCenter 0.5s forwards linear";
    }
}