var AjaxHandler = {
    xhttp: null,
    GETRequest: function(url, onSuccess, onError) {
        var xhttp = new XMLHttpRequest();

        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4) {
                if (xhttp.status == 200) {
                    if (onSuccess != null) {
                        onSuccess(xhttp.responseText);
                    }
                } else {
                    if (onError != null) {
                        onError(xhttp.responseText);
                    }
                }

            }
        }
        xhttp.open('GET', url, true);
        xhttp.send();
    },
    POSTRequest: function(url, data, headers, onSuccess, onError) {
        this.xhttp = new XMLHttpRequest();
        this.xhttp.onreadystatechange = function() {
            if (this.readyState == 4) {
                if (this.status == 200) {

                    if (onSuccess != null) {

                        onSuccess(this.responseText);
                    }
                } else {
                    if (onError != null) {
                        onError(this.responseText);
                    }
                }
            }
        }

        this.xhttp.open('POST', url);
        for (header of headers) {
            this.xhttp.setRequestHeader(header.header, header.value);
        }
        this.xhttp.send(data);
    }
}