function Logout() {
    url = "logout.php";
    AjaxHandler.GETRequest(url, function() {
        window.location = "game_shop.php";
    }, function(e) {
        console.log('error: ' + e);
    })
}

function getCart() {
    let element1 = document.getElementById('cartItemContainer');
    element1.style.display = "block";
    if (element1.classList.contains("slideUp")) {
        element1.classList.remove("slideUp");
        element1.classList.add("slideDown");
    } else {
        element1.classList.remove("slideDown");
        element1.classList.add("slideUp");
        return;
    }
    getCartItems();
}

function getCartItems() {
    let url = "shopping_cart.php";
    var headers = [{ header: "Content-Type", value: "application/x-www-form-urlencoded" }];
    let data = "get_cart=1";
    AjaxHandler.POSTRequest(url, data, headers, function(result) {
        let cartList = document.getElementById('cartList');
        cartList.innerHTML = result;
        let deleteItems = document.getElementsByClassName("deleteItem");

        for (let i = 0; i < deleteItems.length; i++) {
            let element = deleteItems[i];
            let id = element.getAttribute('data-code');
            element.addEventListener('click', function(event) {
                removeCartItem(id);
                getCartItems();
            });
        }

    })
}

function removeCartItem(id, callback) {
    let url = 'shopping_cart.php?remove_id=' + id;
    AjaxHandler.GETRequest(url, function(result) {
        let element = document.getElementById('shoppingCartNum');
        element.innerHTML = JSON.parse(result).items;
        if (callback) {
            callback();
        }
    })
}

function addToCart() {
    let url = "shopping_cart.php";
    var headers = [{ header: "Content-Type", value: "application/x-www-form-urlencoded" }];
    let id = document.getElementById("product_id").value;
    data = "product_id=" + id;
    AjaxHandler.POSTRequest(url, data, headers, function(response) {
        let element = document.getElementById('shoppingCartNum');
        element.innerHTML = JSON.parse(response).items;
        getCartItems();
    });
}

function toProfile(event) {
    let id = event.target.getAttribute('data-code');
    window.location = "user_edit.php";
}

function showUserButtons() {
    let element = document.getElementById('userContainer');
    element.style.display = "block";
    if (element.classList.contains("slideUp")) {
        element.classList.remove("slideUp");
        element.classList.add("slideDown");
    } else {
        element.classList.remove("slideDown");
        element.classList.add("slideUp");
        return;
    }
}

let userButton = document.getElementById('userButton');
if (userButton != null) {
    userButton.addEventListener('click', showUserButtons);
}
let shopButton = document.getElementById('shopEditButton');
if (shopButton != null) {
    shopButton.addEventListener('click', function() {
        window.location = "shop_edit.php";
    })
}

let purchaseButton = document.getElementById('orderButton');
if(purchaseButton != null) {
    purchaseButton.addEventListener('click',function(){
        window.location = 'order.php';
    });
}

let profileButton = document.getElementById('profileButton');
if (profileButton != null) {
    profileButton.addEventListener('click', toProfile);
}

let logoutButton = document.getElementById('logoutButton');
if (logoutButton != null) {
    logoutButton.addEventListener('click', Logout);
}

let shoppingCart = document.getElementById('shoppingCart');
if (shoppingCart != null) {
    shoppingCart.addEventListener('click', getCart);
}