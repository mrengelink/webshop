function deleteUser() {
    let url = "user_edit.php";
    let data = "delete=1";
    let headers = [{ header: "Content-Type", value: "application/x-www-form-urlencoded" }];
    if (confirm("You are about to permanently delete your account. Are you sure ?")) {
        AjaxHandler.POSTRequest(url, data, headers, function(result) {
            window.location = "logout.php";
        });
    }
}
let element = document.getElementById('deleteButton');
if (element != null) {
    element.addEventListener('click', deleteUser);
}