function BindButtons() {
    let shopElements = document.getElementsByClassName('shopItem');
    for (element of shopElements) {
        let id = element.getAttribute('data-code');
        element.addEventListener('click', function() {
            window.location = 'game_info.php?id=' + id;
        })
    }
}
BindButtons();

let timer = null;
let searchElement = document.getElementById('searchField');
searchElement.addEventListener('keyup', function() {
    let waitTime = 300; //ms

    clearTimeout(timer);

    timer = setTimeout(function() {
        let input = searchElement.value;
        let url = "game_shop.php?q=" + input;
        AjaxHandler.GETRequest(url, function(response) {
            let list = document.getElementById('shopList');
            list.innerHTML = response;
            BindButtons();
        })
    }, waitTime);
});