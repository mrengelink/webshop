let addProduct = document.getElementById('addProductButton');
if (addProduct != null) {
    addProduct.addEventListener('click', function() {
        let form = document.getElementById('addProductForm');
        form.submit();
    })
}


let timer = null;
let searchElement = document.getElementById('searchField');
searchElement.addEventListener('keyup', function() {
    let waitTime = 300; //ms

    clearTimeout(timer);

    timer = setTimeout(function() {
        let input = searchElement.value;
        let url = "shop_edit.php?q=" + input;
        AjaxHandler.GETRequest(url, function(response) {
            console.log(response);
            let list = document.getElementById('productList');
            list.innerHTML = response;
            //BindButtons();
        })
    }, waitTime);
});

let selectProduct  = function (id) {
    let url = "shop_edit.php?product_id=" + id;
    AjaxHandler.GETRequest(url, function (response){
        let container = document.getElementById('shopEdit');
        container.innerHTML = response;
        BindButtons();
    })
}
function BindButtons() {
    let elements = document.getElementsByClassName('product');
    for(let e of elements) {
        let id = e.getAttribute('data-code');
        e.addEventListener('click',function (){
            selectProduct(id);
        },false);
    }
    let cancelButton = document.getElementById('cancelButton');
    if(cancelButton){
        cancelButton.addEventListener('click', () => {
            window.location = "shop_edit.php";
        });
    }
    let editButton = document.getElementById('editButton');
    if(editButton) {
        editButton.addEventListener('click', () => {
            let form = document.getElementById('editform');
            form.submit();
        })
    }

    let deleteButton = document.getElementById('deleteButton');
    if(deleteButton) {
        deleteButton.addEventListener('click', () => {
            let url = "shop_edit.php";
            let data = "delete=1";
            let headers = [{ header: "Content-Type", value: "application/x-www-form-urlencoded" }];
            AjaxHandler.POSTRequest(url,data,headers,(response) => {
                window.location= "shop_edit.php";
            })
        });
    } 
}
BindButtons();
