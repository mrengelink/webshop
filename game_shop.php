<?php 
    include('dbHandler.php');
    include('util.php');
    $title="Shop";

    /**************************************************
     * Search Query
     *************************************************/
    if(isset($_GET['q'])){
        if($_GET['q'] == ''){
            $sql= "SELECT * FROM games LIMIT 10";
        }else{
            $sql = "SELECT * FROM games WHERE TITLE LIKE ? LIMIT 3";
        }
        
        $q = Util::validateInput ( $_GET['q']);
        $q = "%$q%";
        
        $result = DbHandler::Query($sql,[$q]);
        $shopList = '';
        foreach($result as $row) {
            $shopList .= createShopItem($row);
        }
        die($shopList);
    }

    /**************************************************
     *  Select all and show them
     **************************************************/

    $sql= "SELECT * FROM games";
    $result = DbHandler::Query($sql);
    $shopList = '';
    foreach($result as $row){
        $shopList .= createShopItem($row);    
    }
    function createShopItem ($item) {
        $id = $item['ID'];
        $gameTitle = $item['TITLE'];
        $price = $item['PRICE'];
        $desc = $item['DESCRIPTION'];
        $thumb = $item['THUMB'];
        $html = "<div class='shopItem buttonCursor' data-code='$id'><img src='$thumb'><div><div class='shopTitle' >$gameTitle</div><div class='shopPrice'>&euro; $price</div><div class='shopDesc'>$desc</div></div></div>";
        return $html;
    }


    include('html/head.html');
    include('header.php');
    include('html/game_shop.html');
    include('html/footer.html');
?>
