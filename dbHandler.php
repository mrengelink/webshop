<?php 
Class DbHandler {
    private static $conn;
    private static $host = "localhost";
    private static $dbName = "login_test";
    private static $dbUser = "test";
    private static $dbPass = "test";
    private static $dbCharset = "";
    private static $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ];
    private static $initialized = false;
    
    private function createConnection () {
        if(self::$initialized){
            return;
        }
        $dsn = "mysql:host=". self::$host . ";dbname=" . self::$dbName;
        try {
            self::$conn = new PDO($dsn,self::$dbUser,self::$dbPass);
        }catch (\PDOExeption $e) {
            throw new \PDOException($e->getMessage(),(int)$e->getCode());
        }
    }
    
    static function Query ($sql,$variables = []) {
        self::createConnection();
        try{
            $statement = self::$conn->prepare($sql);
            $statement->execute($variables);
            $result = $statement->fetchAll();
        } catch(\PDOExeption $e) {
            throw new \PDOException($e->getMessage(),(int)$e->getCode());
        }
        return $result;
    }

    static function MultiQuery($queryList = []){
        $statements = [];
        $results = [];
        self::createConnection();
        try{
            self::$conn->beginTransaction();
            for($i=0; $i < count($queryList); $i++) {
                $statements[$i] = self::$conn->prepare($queryList[$i]['sql']);
            }
            $lastID=null;
            for($i=0; $i < count($queryList); $i++) {
                foreach($queryList[$i]['var'] as $var => $k) {
                    if($var == 'LAST_ID'){
                        if(!$lastID){
                            $lastID = self::$conn->lastInsertId();
                        }
                        $queryList[$i]['var']['LAST_ID'] = $lastID;
                    }
                }
                $statements[$i]->execute($queryList[$i]['var']);
                $results[$i] = $statements[$i]->fetchAll();
            }
            
             
            self::$conn->commit();
            return $results;
        } catch(\PDOExeption $e){
            self::$conn->rollBack();
            throw new \PDOException($e->getMessage(),(int)$e->getCode());
        }
    }
}
?>