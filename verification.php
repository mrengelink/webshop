<?php
include ("dbHandler.php");
$title="Account Verification";

if($_SERVER['REQUEST_METHOD'] == "GET") {
    $token = $_GET['token'];
    $sql = "UPDATE users SET ACTIVE=1,TOKEN=NULL WHERE TOKEN=?";
    DbHandler::Query($sql,$token);
}

include ('html/head.html');
include ('header.php');
include ('html/verification.html');
include ('html/footer.html');
?>