<?php
include('dbHandler.php');
session_start();
/***********************************************
 *  Add items to card
 **********************************************/
if(isset($_POST['product_id'])){
    foreach($_POST as $key => $value){
        $product[$key] = filter_var($value, FILTER_SANITIZE_STRING);
    }
    $id =$_POST['product_id'];
    $sql = "SELECT TITLE,PRICE,THUMB FROM games WHERE ID=?";
    $result = DbHandler::Query($sql,[$id]);
    $row = $result[0];
    
    $product['title'] = $row['TITLE'];
    $product['price'] = $row['PRICE'];
    $product['thumb'] = $row['THUMB'];

    if(isset($_SESSION['products'])){
        if(isset($_SESSION['products'][$product['product_id']])){
            unset($_SESSION['products'][$product['product_id']]);
        }
    }

    $_SESSION['products'][$product['product_id']] = $product;
    $total = count($_SESSION['products']);
    die(json_encode(array('items'=>$total)));
}

/******************************************************
 *  Remove item from card
 ******************************************************/
if(isset($_GET['remove_id']) && isset($_SESSION['products'])){
    $gameID = filter_var( $_GET['remove_id'] , FILTER_SANITIZE_STRING );
    if(isset($_SESSION['products'][$gameID])){
        unset($_SESSION['products'][$gameID]);
    }
    $total = count($_SESSION['products']);
    die(json_encode(array('items'=>$total)));
}
/******************************************************
 * List items in cart
 *****************************************************/
if(isset($_POST['get_cart']) && $_POST['get_cart'] == 1){
    if(isset($_SESSION['products']) && count($_SESSION['products']) > 0){
        $cart = "";//"<ul class=''>";
        $total = 0;
        foreach($_SESSION['products'] as $product ){
            $title = $product['title'];
            $price = $product['price'];
            $id = $product['product_id'];

            $total += $price;
            $cart .= "<li><span class='cartTitle'>$title</span><span class='cartPrice'>&euro;$price</span><i class='buttonCursor deleteItem fas fa-times' data-code='$id'></i></li>";
        }
        $cart .= "<li><span class='cartTotal'>Total:</span><span>&euro;$total</span></li>";
        die($cart);
    }else{
        die("Your cart is empty");
    }
}


?>