<?php
    session_start();
    include('util.php');
    include('dbHandler.php');
    $id= $_SESSION['ID'];

    // Change contact info
    if(!empty($_POST['email'])) {
        $variables = [];
        foreach($_POST as $k => $v){
            $variables[":".$k] = Util::validateInput($v);
        } 
        $variables[':id'] = $id;
        $sql = "UPDATE users SET firstname=:firstname, lastname=:lastname , email=:email WHERE id=:id";
        $result = DbHandler::Query($sql,$variables);
    }
    /****************************************
     * Delete Account
     ****************************************/
    
    if(isset($_POST['delete']) && $_POST['delete'] == 1) {
        $sql = "DELETE FROM users WHERE id=?";
        $result = DbHandler::Query($sql,[$id]);
    }

    $sql = "SELECT FIRSTNAME,LASTNAME,EMAIL FROM users WHERE id=?";
    $result = DbHandler::Query($sql,[$id]);
    $r = $result[0];
    $title = "Edit User";

    $firstName = $r['FIRSTNAME'];
    $lastName = $r['LASTNAME'];
    $email = $r['EMAIL'];
    $pass = '*******';
    


    include('html/head.html');
    include('header.php');
    include('html/user_edit.html');
    include('html/footer.html');
?>
