<?php 
    include('dbHandler.php');
    session_start();
    
    $order = "No products in shopping cart";
    if(isset($_SESSION['products'])){
        $order = "";
        $products = $_SESSION['products'];
        $total = 0;
        foreach($products as $product){
            $title = $product['title'];
            $price = $product['price'];
            $thumb = $product['thumb'];
            $total += $price;
            $order .= "<div class='orderItem'><img src='$thumb'><div><span>$title</span><span>$price</span><i class='buttonCursor deleteItem fas fa-times'></i></div></div>";
        }
        $order .= "<div><span>Total:</span><span>$total</span></div>";
    }


    include('html/head.html');
    include('header.php');
    include('html/order.html');
    include('html/footer.html');
?>